import React, { Component } from 'react';
import * as gameActions from 'src/actions/game';
import Layout from "src/components/layout";
import * as validation from "src/ultils/validation";
import { EResourceType } from 'src/enum';


class AddResult extends Component<any, any> {

  constructor(props: any) {
    super(props);

    this.state = {
      resourceDate: [],
      fields_invalid: {},
      data: {},
    }

  }

  formRef: any = React.createRef();

  componentDidMount = () => {
    gameActions.getResourceData(EResourceType.date).then((r: any) => {
      this.setState({
        resourceDate: r.data,
      });
    }, (err: any) => {
      alert(err.message)
    });

  }

  handleChange = (e) => {
    var fieldName = e.target.name;
    var fieldValue = e.target.value;

    let isValid = true;

    switch (fieldName) {
      case 'phone': isValid = validation.validateOnlyNum(fieldValue); break;
      default: isValid = validation.validateInput(fieldValue); break;
    }

    let { data, fields_invalid } = this.state;

    if (isValid) {
      data[fieldName] = fieldValue;
      fields_invalid[`invalid_${fieldName}`] = false;
      this.setState({
        data: data,
        fields_invalid: fields_invalid
      });
    }
    else {
      fields_invalid[`invalid_${fieldName}`] = true;
      this.setState({
        fields_invalid: fields_invalid
      });
    }

  }

  addResult = (e) => {
    e.preventDefault();

    const { data, fields_invalid } = this.state;

    let keyInvalid = '';

    for (let key in fields_invalid) {
      if (fields_invalid[key]) {
        keyInvalid = key;
        break;
      }
    }

    if (keyInvalid !== '') {
      alert(keyInvalid);
    }
    else {
      data.type = EResourceType[EResourceType.result];
      gameActions.setResourceData(data).then((r: any) => {
        alert('Thêm kết quả sổ số thành công.');
        this.resetForm();
      }, (err: any) => {
        alert(err.message);
      });
    }

  }

  resetForm = () => {
    this.formRef.reset();
  }

  render() {
    const { resourceDate, fields_invalid } = this.state;

    return (
      <Layout>
        <form className="gamify__body__add-result" onSubmit={this.addResult} ref={form => this.formRef = form}>
          <h1>Nhập kết quả trúng thưởng</h1>
          <div className="form-group">
            <label >Ngày xổ Loto:</label>
            <select className="form-control" name="resource_date_id" onChange={this.handleChange}>
              <option value='-1'>-- Chọn ngày --</option>
              {resourceDate.map((item, index) => (
                <option key={index} value={item.id}>{item.date}</option>
              ))}
            </select>
          </div>
          <div className="form-group">
            <label>Họ và tên</label>
            <input type="text" className={`form-control ${fields_invalid.invalid_username ? 'is-invalid' : ''}`} name="fullname" onChange={this.handleChange} required />
            <small className={`text-danger ${fields_invalid.invalid_username ? '' : 'd-none'}`} >Chứa ký tự đặt biệt không cho phép.</small>
          </div>
          <div className="form-group">
            <label>Số điện thoại</label>
            <input type="number" className={`form-control ${fields_invalid.invalid_password ? 'is-invalid' : ''}`} name="phone" onChange={this.handleChange} required />
            <small className={`text-danger ${fields_invalid.invalid_password ? '' : 'd-none'}`} >Mục này chỉ cho phép nhập ký tự số.</small>
          </div>
          <button type="submit" className="btn btn-primary">Thêm</button>
        </form>
      </Layout>
    );
  }
}

export default AddResult;