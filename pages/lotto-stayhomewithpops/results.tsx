import React, { Component } from 'react';
import * as gameActions from 'src/actions/game';
import Layout from "src/components/layout";
import { EResourceType } from 'src/enum';

class Results extends Component<any, any> {

  constructor(props: any) {
    super(props);

    this.state = {
      resourceResult: [],
      resourceDate: [],
      selectedDate: null,
    }

  }

  componentDidMount = () => {
    this.getAllResourceResults();

    gameActions.getResourceData(EResourceType.date).then((r: any) => {
      this.setState({
        resourceDate: r.data,
      });
    }, (err: any) => {
      alert(err.message)
    });

  }

  getAllResourceResults = () => {
    gameActions.getResourceData(EResourceType.result).then((r: any) => {
      this.setState({
        resourceResult: r.data,
      });
    }, (err: any) => {
      alert(err.message)
    });
  }

  getResourceResultsByDate = () => {
    const { selectedDate } = this.state; console.log('selectedDate 999', selectedDate);
    if(!selectedDate || selectedDate == '-1') {
      this.getAllResourceResults();
    }
    gameActions.getResourceDataByDate(EResourceType.result, selectedDate).then((r: any) => {
      this.setState({
        resourceResult: r.data,
      });
    }, (err: any) => {
      alert(err.message)
    });
  }

  handleChange = (e) => {
    var fieldValue = e.target.value;
    this.setState({
      selectedDate: fieldValue,
    }, () => this.getResourceResultsByDate());
  }

  render() {
    const { resourceResult, resourceDate } = this.state;

    return (
      <Layout>
        <div className="gamify__body__results">
          <h1>Danh sách trúng thưởng</h1>
          <div className="form-group">
            <label >Ngày xổ Loto:</label>
            <select className="form-control" name="resource_date_id" onChange={this.handleChange}>
              <option value='-1'>-- Chọn ngày --</option>
              {resourceDate.map((item, index) => (
                <option key={index} value={item.id}>{item.date}</option>
              ))}
            </select>
          </div>
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>STT</th>
                <th>Họ và tên</th>
                <th>Số điện thoại</th>
              </tr>
            </thead>
            <tbody>
              {resourceResult.length > 0 ? resourceResult.map((item, index) => (
                <tr key={`result-${index}`}>
                  <td>{index}</td>
                  <td>{item.fullname}</td>
                  <td>{item.phone}</td>
                </tr>
              )) : (
                <tr>
                  <td colSpan={3} className="text-center">Không có kết quả ứng với ngày đã chọn</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </Layout>
    );
  }
}

export default Results;