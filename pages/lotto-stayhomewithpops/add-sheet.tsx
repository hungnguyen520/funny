import React, { Component } from 'react';
import * as gameActions from 'src/actions/game';
import Layout from "src/components/layout";
import * as validation from "src/ultils/validation";
import * as authService from 'src/ultils/auth';
import SelectLoto from 'src/components/select-loto';
import DetailLoto from 'src/components/detail-loto';
import Router from 'next/router';
import { PATHS } from 'src/constant';


class AddSheet extends Component {
  render() {
    return (
      <Layout>
        Add sheet
      </Layout>
    );
  }
}

export default AddSheet;