import React, { Component } from 'react';
import * as gameActions from 'src/actions/game';
import Layout from "src/components/layout";
import * as validation from "src/ultils/validation";
import Router from 'next/router';
import { PATHS } from 'src/constant';

interface ISignupState {
  fullname: string;
  birthday: string;
  email: string;
  phone: string;
  cmnd: string;
  address: string;
  username: string;
  password: string;
}

class Signup extends Component<any, any> {

  constructor(props: any) {
    super(props);

    this.state = {
      data: {},
      fields_invalid: {},
    };
    
  }

  handleChange = (e) => {
    var fieldName = e.target.name;
    var fieldValue = e.target.value;

    let isValid = true;

    switch(fieldName) {
      case 'birthday': isValid = validation.validateDate(fieldValue); break;
      case 'email': isValid = validation.validateEmail(fieldValue); break;
      case 'phone':
      case 'cmnd': isValid = validation.validateOnlyNum(fieldValue); break;
      case 'password': isValid = validation.validatePassword(fieldValue); break;
      case 'username': isValid = validation.validateUsername(fieldValue); break;
      default: isValid = validation.validateInput(fieldValue); break;
    }

    let { data, fields_invalid } = this.state;

    if(isValid) {
      data[fieldName] = fieldValue;
      fields_invalid[`invalid_${fieldName}`] = false;
      this.setState({
        data: data,
        fields_invalid: fields_invalid
      });
    }
    else {
      fields_invalid[`invalid_${fieldName}`] = true;
      this.setState({
        fields_invalid: fields_invalid
      });
    }

  }



  register = (e) => {
    e.preventDefault();
    const { data, fields_invalid } = this.state;

    let keyInvalid = '';

    for(let key in fields_invalid) {
      if(fields_invalid[key]){
        keyInvalid = key;
        break;
      }
    }

    if(keyInvalid !== '') {
      alert(keyInvalid);
    }
    else {
      gameActions.registerUser(data).then((r: any) => {
        Router.push(
          `${PATHS.SIGNIN}`,
          `${PATHS.SIGNIN}`
        );
      }, (err: any) => {
        alert(err.message);
      });
    }
  
  }


  render() {

    const { fields_invalid } = this.state;

    return (
      <Layout>
        <form className="gamify__body__signup" onSubmit={this.register}>
          <div className="form-group">
            <label>Họ và tên:</label>
            <input type="text" className={`form-control ${fields_invalid.invalid_fullname ? 'is-invalid' : ''}`} name="fullname" onChange={this.handleChange} required/>
            <small className={`text-danger ${fields_invalid.invalid_fullname ? '' : 'd-none'}`} >Chứa ký tự đặt biệt không cho phép.</small>
          </div>
          <div className="form-group has-error">
            <label>Sinh nhật:</label>
            <input type="text" className={`form-control ${fields_invalid.invalid_birthday ? 'is-invalid' : ''}`} name="birthday" onChange={this.handleChange} required/>
            <small className={`text-danger ${fields_invalid.invalid_birthday ? '' : 'd-none'}`} >Chưa đúng định dạng ngày DD/MM/YYYY.</small>
          </div>
          <div className="form-group">
            <label>Email:</label>
            <input type="email" className={`form-control ${fields_invalid.invalid_email ? 'is-invalid' : ''}`} name="email" onChange={this.handleChange} required/>
            <small className={`text-danger ${fields_invalid.invalid_email ? '' : 'd-none'}`} >Chưa đúng định dạng email.</small>
          </div>
          <div className="form-group">
            <label>Số điện thoaị:</label>
            <input type="text" className={`form-control ${fields_invalid.invalid_phone ? 'is-invalid' : ''}`} name="phone" onChange={this.handleChange} required/>
            <small className={`text-danger ${fields_invalid.invalid_phone ? '' : 'd-none'}`} >Chỉ nhập ký tự số ở mục này.</small>
          </div>
          <div className="form-group">
            <label>CMND/CCCD:</label>
            <input type="text" className={`form-control ${fields_invalid.invalid_cmnd ? 'is-invalid' : ''}`}  name="cmnd" onChange={this.handleChange} required/>
            <small className={`text-danger ${fields_invalid.invalid_cmnd ? '' : 'd-none'}`} >Chỉ nhập ký tự số ở mục này.</small>
          </div>
          <div className="form-group">
            <label>Địa chỉ:</label>
            <input type="text" className={`form-control ${fields_invalid.invalid_address ? 'is-invalid' : ''}`} name="address" onChange={this.handleChange} required/>
            <small className={`text-danger ${fields_invalid.invalid_address ? '' : 'd-none'}`} >Chứa ký tự đặt biệt không cho phép.</small>
          </div>
          <div className="form-group">
            <label>ID:</label>
            <input type="text" className={`form-control ${fields_invalid.invalid_username ? 'is-invalid' : ''}`} name="username" onChange={this.handleChange} required/>
            <small className={`text-danger ${fields_invalid.invalid_username ? '' : 'd-none'}`} >Chứa ký tự đặt biệt, khoảng trắng hoặc dấu TV.</small>
          </div>
          <div className="form-group">
            <label>Password:</label>
            <input type="password" className={`form-control ${fields_invalid.invalid_password ? 'is-invalid' : ''}`} name="password" onChange={this.handleChange} required/>
            <small className={`text-danger ${fields_invalid.invalid_password ? '' : 'd-none'}`} >Chứa ký tự đặt biệt không cho phép hoặc ít hơn 6 ký tự.</small>
          </div>
          <button type="submit" className="btn btn-primary">Đăng ký</button>
        </form>
      </Layout>
    );
  }
}

export default Signup;