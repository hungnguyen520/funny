import React, { Component } from 'react';
import * as gameActions from 'src/actions/game';
import Layout from "src/components/layout";
import * as validation from "src/ultils/validation";
import Router from 'next/router';
import { PATHS } from 'src/constant';

class Signin extends Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      data: {},
      fields_invalid: {},
    };
    
  }

  handleChange = (e) => {
    var fieldName = e.target.name;
    var fieldValue = e.target.value;

    let isValid = true;

    switch(fieldName) {
      case 'password': isValid = validation.validatePassword(fieldValue); break;
      case 'username': isValid = validation.validateUsername(fieldValue); break;
      default: isValid = validation.validateInput(fieldValue); break;
    }

    let { data, fields_invalid } = this.state;

    if(isValid) {
      data[fieldName] = fieldValue;
      fields_invalid[`invalid_${fieldName}`] = false;
      this.setState({
        data: data,
        fields_invalid: fields_invalid
      });
    }
    else {
      fields_invalid[`invalid_${fieldName}`] = true;
      this.setState({
        fields_invalid: fields_invalid
      });
    }

  }


  login = (e) => {
    e.preventDefault();

    const { data, fields_invalid } = this.state;

    let keyInvalid = '';

    for(let key in fields_invalid) {
      if(fields_invalid[key]){
        keyInvalid = key;
        break;
      }
    }

    if(keyInvalid !== '') {
      alert(keyInvalid);
    }
    else {
      gameActions.loginUser(data.username, data.password).then((r: any) => {
        Router.push(
          `${PATHS.SELECTION}`,
          `${PATHS.SELECTION}`
        );
      }, (err: any) => {
        alert(err.message);
      });
    }

  }

  render() {
    const { fields_invalid } = this.state;

    return (
      <Layout>
        <form className="gamify__body__signin" onSubmit={this.login}>
          <div className="form-group">
            <label>ID:</label>
            <input type="text" className={`form-control ${fields_invalid.invalid_username ? 'is-invalid' : ''}`} name="username" onChange={this.handleChange} required />
            <small className={`text-danger ${fields_invalid.invalid_username ? '' : 'd-none'}`} >Chứa ký tự đặt biệt, khoảng trắng hoặc dấu TV.</small>
          </div>
          <div className="form-group">
            <label>Password:</label>
            <input type="password" className={`form-control ${fields_invalid.invalid_password ? 'is-invalid' : ''}`} name="password" onChange={this.handleChange} required />
            <small className={`text-danger ${fields_invalid.invalid_password ? '' : 'd-none'}`} >Chứa ký tự đặt biệt không cho phép hoặc ít hơn 6 ký tự.</small>
          </div>
          <button type="submit" className="btn btn-primary">Đăng nhập</button>
        </form>
      </Layout>
    );
  }
}

export default Signin;