import React, { Component } from 'react';
import * as gameActions from 'src/actions/game';
import Layout from "src/components/layout";
import * as validation from "src/ultils/validation";
import * as authService from 'src/ultils/auth';
import SelectLoto from 'src/components/select-loto';
import DetailLoto from 'src/components/detail-loto';
import Router from 'next/router';
import { PATHS } from 'src/constant';

class Selection extends Component<any, any> {

  constructor(props: any) {
    super(props);

    this.state = {
      selectionDetail: null,
    }

  }

  componentDidMount = () => {
    if(!authService.isAuth()) {
      Router.push(
        `${PATHS.SIGNIN}`,
        `${PATHS.SIGNIN}`
      );
    } else {
      this.getSelectionDetail();
    }
  }

  getSelectionDetail = () => {
    const { selectedDetail } = this.state;
    if(selectedDetail) return;

    const userDetail = authService.getAuthDetail();
    gameActions.getSelectionLoto(userDetail.username).then((r:any) => {
      let listSelection = r.data;
      let listId = listSelection.map((item) => {
        return item.id;
      });

      listId.sort();

      let selectionDetail = null;
      
      listSelection.forEach((item) => {
        if(item.id === listId[0])
          selectionDetail = item;
      });

      this.setState({
        selectionDetail: selectionDetail,
      });

    }, (err:any) => {
      alert(err.message);
    });
  }


  render() {
    const { selectionDetail } = this.state;
    return (
      <Layout>
        {selectionDetail ? (<DetailLoto data={selectionDetail} />) : (<SelectLoto />)}
      </Layout>
    );
  }
}

export default Selection;