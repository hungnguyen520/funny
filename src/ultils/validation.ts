import moment from 'moment';

export const validateInput = (value: string) => {
  const regex = /(\`|\~|\!|\#|\$|\%|\^|\*|\(|\)|\+|\=|\[|\{|\]|\}|\||\\|\'|\<|\,|\>|\?|\/|\""|\;|\:|\@|\&)/; // allow space/_/./_
  return !regex.test(value);
}

export const validateOnlyNum = (value: string) => {
  const regex = /^[0-9]*$/;
  return regex.test(value);
}

export const validateEmail = (value: string) => {
  const regex = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/
  return regex.test(value);
}

export const validateDate = (value: string) => {
  return moment(value, "DD/MM/YYYY", true).isValid();
}

export const validateUsername = (value: string) => {
  const regex = /^[a-zA-Z0-9_.@]+$/;
  return regex.test(value);
}

export const validatePassword = (value: string) => {
  if(value.length < 6) return false;
  const regex = /^[a-zA-Z0-9_.@&$%!#]+$/;
  return regex.test(value);
}

export default validateInput;