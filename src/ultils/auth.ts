import Cookies from 'universal-cookie';
import * as constants from 'src/constant';
import _ from 'lodash';

const cookies = new Cookies();

export function isAuth() {
  return !_.isUndefined(cookies.get(constants.AUTH_TOKEN));
}

export function getAuthToken() {
  return cookies.get(constants.AUTH_TOKEN);
}

export function getAuthDetail() {
  return cookies.get(constants.AUTH_DETAIL);
}

export default isAuth;