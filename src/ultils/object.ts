export function filter(originalObj: any) {
  const obj = originalObj;
  const keys = Object.keys(obj);
  for(let i = 0; i < keys.length; i+=1) {
    const propName = keys[i];
    if (
      obj[propName] === null ||
      obj[propName] === undefined ||
      obj[propName] === ''
    ) {
      delete obj[propName];
    }
  }
  return obj;
}

export default filter;
