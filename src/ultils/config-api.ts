import Cookies from 'universal-cookie';
import btoa from 'btoa';
import _ from 'lodash';
import * as objectHelper from 'src/ultils/object';
import * as constants from 'src/constant';

const cookies = new Cookies();

export function getLinkAPI(uri: string, opts: any = {isServer: false}) {
  return process.env.APP_API_URL + uri;
}

export function getHeaderRequest(currentAccessToken:any = null, customHeaders: any = {}) {
  let language = cookies.get('next-i18next') || 'vi';
  let defaultHeaders = {};
  let accessToken = currentAccessToken;

  if (accessToken == null && !_.isUndefined(cookies.get(constants.AUTH_TOKEN))) {
    accessToken = cookies.get(constants.AUTH_TOKEN);
  }

  defaultHeaders = objectHelper.filter({
    Authorization: accessToken,
    'accept-language': language,
  });

  return Object.assign(_.pickBy(defaultHeaders, _.identity), customHeaders);
}