const Request = require('superagent');

export function get(url: string, opts:any = {}) {
  const req = Request.get(url);
  const options = Object.assign({withCredentials: false}, opts);
  if (options.withCredentials) {
    req.withCredentials();
  }
  return req;
}

export function post(url: string, opts:any = {}) {
  const req = Request.post(url);
  const options = Object.assign({withCredentials: false}, opts);
  if (options.withCredentials) {
    req.withCredentials();
  }
  return req;
}
