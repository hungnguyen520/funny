import React, { Component } from 'react';
import * as gameActions from 'src/actions/game';
import * as validation from "src/ultils/validation";
import * as authService from 'src/ultils/auth';
import { EResourceType } from 'src/enum';
import Router from 'next/router';
import { PATHS } from 'src/constant';

class SelectLoto extends Component<any, any> {

  constructor(props: any) {
    super(props);

    this.state = {
      resourceDate: null,
      resourceSheet: null,
      selectedDate: null,
      selectedSheet: null,
    }

  }

  componentDidMount = () => {
    gameActions.getResourceData(EResourceType.date).then((r: any) => {
      this.setState({
        resourceDate: r.data,
      });
    }, (err: any) => {
      alert(err.message)
    });

    //===================

    gameActions.getResourceData(EResourceType.sheet).then((r: any) => {
      this.setState({
        resourceSheet: r.data
      });
    }, (err: any) => {
      alert(err.message)
    });
  }

  handleChange = (e) => {
    let fieldName = e.target.name;
    let fieldValue = e.target.value;

    this.setState({
      [fieldName]: fieldValue
    });

  }

  saveSelectionData = () => {
    const { selectedDate, selectedSheet } = this.state;
    if(!selectedDate || !selectedSheet) {
      alert('Bạn hãy chọn ngày và tờ loto nhé !');
      return;
    }

    const userDetail = authService.getAuthDetail();
    if(!userDetail) {
      Router.push(
        `${PATHS.SIGNIN}`,
        `${PATHS.SIGNIN}`
      );
      return;
    }

    let data = {
      username: userDetail.username,
      resource_date_id: selectedDate,
      resource_sheet_id: selectedSheet,
      date: new Date()
    }

    gameActions.saveSelectionLoto(data).then((r:any)=>{
      Router.push(
        `${PATHS.SELECTION}`,
        `${PATHS.SELECTION}`
      );
    }, (err:any) => {
      alert(err.message);
    })
  }

  render() {
    const { resourceDate, resourceSheet } = this.state;

    if (!resourceDate || !resourceSheet) return null;

    return (
      <div className="gamify__body__selection" >
        <h1>Tham gia</h1>
        <div className="form-group">
          <label >Ngày xổ Loto:</label>
          <select className="form-control" name="selectedDate" onChange={this.handleChange}>
            <option value='-1'>-- Chọn ngày --</option>
            {resourceDate.map((item, index) => (
              <option key={index} value={item.id}>{item.date}</option>
            ))}
          </select>
        </div>
        <div className="form-group">
          {resourceSheet.map((item, index) => (
            <div className="form-check-inline" key={index}>
              <label htmlFor={`radio-${index}`} className="form-check-label">
                {/* <img src={item.image_url} /> */}
                <div>
                  <img src='/static/images/loto_sheet.jpg' width={100} />
                </div>
                <input type="radio" className="form-check-input" name="selectedSheet" value={item.id} id={`radio-${index}`} onChange={this.handleChange}/>{item.code}
              </label>
            </div>
          ))}
        </div>
        <div className="form-group">
          <button type="button" className="btn btn-primary" onClick={this.saveSelectionData}>Lưu lựa chọn</button>
        </div>
      </div>
    );
  }
}

export default SelectLoto;