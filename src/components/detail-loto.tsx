import React, { Component } from 'react';
import * as gameActions from 'src/actions/game';
import { EResourceType } from 'src/enum';

class DetailLoto extends Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      dateData: null,
      sheetData: null,
    }

  }

  componentDidMount = () => {
    const { data } = this.props;
    if (!data) return;

    gameActions.getResourceDataById(EResourceType.date, data.resource_date_id).then((r: any) => {
      this.setState({
        dateData: r.data[0]
      });
    }, (err: any) => {
      alert(err.message)
    });

    //================================

    gameActions.getResourceDataById(EResourceType.sheet, data.resource_sheet_id).then((r: any) => {
      this.setState({
        sheetData: r.data[0]
      });
    }, (err: any) => {
      alert(err.message)
    });
  }

  render() {
    const { data } = this.props;
    const { sheetData, dateData } = this.state;

    if (!data || !sheetData || !dateData) return null;

    return (

      <div className="gamify__body__selection" >
        <h1>Xác nhận đăng ký tham gia thành công.</h1>
        <h5>Lựa chọn kỳ sổ số Loto ngày: {dateData.date}</h5>

        <div className="form-group">
          <div>
            <img src='/static/images/loto_sheet.jpg' width={200} />
          </div>
          <label>Code: {sheetData.code}</label>
        </div>
      </div>
    );
  }
}

export default DetailLoto;