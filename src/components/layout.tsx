import React, { Component } from 'react';
import '@style/index.scss';
import "bootstrap/scss/bootstrap.scss";

class Layout extends Component {
  

  render() {
    const { children } = this.props;

    return (
      <div className="gamify">
        <div className="gamify__header">
          HEADER
        </div>
        <div className="gamify__body">
          {children}
        </div>
        <div className="gamify__footer">
          FOOTER
        </div>
      </div>
    );
  }
}

export default Layout;