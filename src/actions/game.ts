import * as request from 'src/ultils/request';
import * as configApi from 'src/ultils/config-api';
import * as constants from 'src/constant';
import Cookies from 'universal-cookie';
import btoa from 'btoa';
import { EResourceType } from 'src/enum';

const cookies = new Cookies();

export function getResourceData(type: EResourceType) {
  const url = configApi.getLinkAPI(`resource?type=${EResourceType[type]}`);
  const headers = configApi.getHeaderRequest();

  return new Promise(function(resolve, reject) {

    request.get(url)
    .set(headers)
    .then(res => {
      if (res.body.status === "S200") {
        resolve(res.body);
      }
      else {
        reject(res.body);
      }
    })
    .catch(error => {
      reject(error);
    });

  });

}

export function getResourceDataById(type: EResourceType, id: string) {
  const url = configApi.getLinkAPI(`resource?type=${EResourceType[type]}&id=${id}`);
  const headers = configApi.getHeaderRequest();

  return new Promise(function(resolve, reject) {

    request.get(url)
    .set(headers)
    .then(res => {
      if (res.body.status === "S200") {
        resolve(res.body);
      }
      else {
        reject(res.body);
      }
    })
    .catch(error => {
      reject(error);
    });

  });

}

export function getResourceDataByDate(type: EResourceType, dateId: string) {
  const url = configApi.getLinkAPI(`resource?type=${EResourceType[type]}&resource_date_id=${dateId}`);
  const headers = configApi.getHeaderRequest();

  return new Promise(function(resolve, reject) {

    request.get(url)
    .set(headers)
    .then(res => {
      if (res.body.status === "S200") {
        resolve(res.body);
      }
      else {
        reject(res.body);
      }
    })
    .catch(error => {
      reject(error);
    });

  });

}

export function setResourceData(data: any) {
  const url = configApi.getLinkAPI('s/content/resource');
  const headers = configApi.getHeaderRequest();

  return new Promise(function(resolve, reject) {

    request.post(url)
    .set(headers)
    .send(data)
    .then(res => {
      if (res.body.status === "S200") {
        resolve(res.body);
      }
      else {
        reject(res.body);
      }
    })
    .catch(error => {
      reject(error);
    });

  });

}

export function registerUser(data: any) {
  const url = configApi.getLinkAPI(`user/${data.username}`);
  const headers = configApi.getHeaderRequest();

  return new Promise(function(resolve, reject) {

    request.post(url)
    .set(headers)
    .send(data)
    .then(res => {
      if (res.body.status === "S200") {
        resolve(res.body);
      }
      else {
        reject(res.body);
      }
    })
    .catch(error => {
      reject(error);
    });

  });

}

export function loginUser(username: string, password: string) {
  const url = configApi.getLinkAPI('s/user');
  const token = 'Basic ' + btoa(`${username}:${password}`)
  const headers = configApi.getHeaderRequest(token);

  return new Promise(function(resolve, reject) {

    request.get(url)
    .set(headers)
    .then(res => {
      if (res.body.status === "S200") {
        cookies.set(constants.AUTH_TOKEN, token);
        cookies.set(constants.AUTH_DETAIL, res.body.data);
        resolve(res.body);
      }
      else {
        reject(res.body);
      }
    })
    .catch(error => {
      reject(error);
    });

  });

}

export function getSelectionLoto(username: string) {
  const url = configApi.getLinkAPI('s/content/selection?username=' + username);
  const headers = configApi.getHeaderRequest();

  return new Promise(function(resolve, reject) {

    request.get(url)
    .set(headers)
    .then(res => {
      if (res.body.status === "S200") {
        resolve(res.body);
      }
      else {
        reject(res.body);
      }
    })
    .catch(error => {
      reject(error);
    });

  });

}

export function saveSelectionLoto(data: any) {
  const url = configApi.getLinkAPI('s/content/selection');
  const headers = configApi.getHeaderRequest();

  return new Promise(function(resolve, reject) {

    request.post(url)
    .set(headers)
    .send(data)
    .then(res => {
      if (res.body.status === "S200") {
        resolve(res.body);
      }
      else {
        reject(res.body);
      }
    })
    .catch(error => {
      reject(error);
    });

  });

}


export function getSelectionList() {
  const url = configApi.getLinkAPI('s/content/selection');
  const headers = configApi.getHeaderRequest();

  return new Promise(function(resolve, reject) {

    request.get(url)
    .set(headers)
    .then(res => {
      if (res.body.status === "S200") {
        resolve(res.body);
      }
      else {
        reject(res.body);
      }
    })
    .catch(error => {
      reject(error);
    });

  });

}