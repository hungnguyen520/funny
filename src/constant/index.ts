export const AUTH_TOKEN = "Pops_Gamify_Auth_Token";
export const AUTH_DETAIL = "Pops_Gamify_Auth_Detail";

export const PATHS = {
  ROOT: process.env.ROOT_PATH,
  SIGNIN: process.env.ROOT_PATH + '/signin',
  SIGNUP: process.env.ROOT_PATH + '/signup',
  SELECTION: process.env.ROOT_PATH + '/selection',
  RESULTS: process.env.ROOT_PATH + 'results',
  ADD_RESULT: process.env.ROOT_PATH + 'add-result',
}